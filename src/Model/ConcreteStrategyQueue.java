package Model;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy {
    @Override
    public void addClient(List<Coada> queues, Client c) {
        Coada minQueue = null;
        int min = Integer.MAX_VALUE;
        for (Coada q : queues) {
            if(q.getSize() < min){
                min = q.getSize();
                minQueue = q;
            }
        }
        minQueue.addClient(c);
    }
}
