package Model;

import java.util.List;

public interface Strategy {
    public void addClient(List<Coada> queues, Client c);
}
