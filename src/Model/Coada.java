package Model;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Coada implements Runnable{

    private volatile boolean exit = false;

    private BlockingQueue<Client> clients;
    private AtomicInteger waitingPeriod;
    private int processingTime;
    private List<Integer> waitingTime;
    private List<Boolean> emptyQueue;
    private int qId;
    private int time = 0;
    private JTextArea logger;
    private JTextField queueState;
    public List<Integer> numberOfClientsInQueue;



    public Coada(int maxNoClients, JTextArea logger, JTextField queueState) {
        clients = new ArrayBlockingQueue<>(maxNoClients);
        waitingPeriod = new AtomicInteger(1000);
        processingTime = 0;
        waitingTime = new ArrayList<>();
        emptyQueue = new ArrayList<>();
        numberOfClientsInQueue = new ArrayList<>();
        this.logger = logger;
        this.queueState = queueState;
    }

    @Override
    public void run() {
        while(!exit){
            if(!this.clients.isEmpty()) {
                emptyQueue.add(true);
                try {
                    Client c = this.clients.peek();

                    for(int i=0;i<c.getProcessingTime()-1;i++) {
                        numberOfClientsInQueue.add(clients.size());
                        Thread.sleep(waitingPeriod.get());
                        waitingTime.add(this.processingTime);
                        this.processingTime--;
                        time++;
                    }
                    this.removeClient();
                    logger.append("Client " + c.getId() + " has left the queue: " + this.getqId() + " at the time " + time + "\n");
                    updateQueueStatus();
                    Thread.sleep(waitingPeriod.get());
                    waitingTime.add(this.processingTime);
                    this.processingTime--;
                    time++;
                } catch (InterruptedException e) {
                    System.out.println(e.getMessage());
                }
            } else {
                emptyQueue.add(false);
                this.numberOfClientsInQueue.add(0);
                waitingTime.add(0);
                try {
                    Thread.sleep(waitingPeriod.get());
                    time++;
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
    }

    public int getProcessingTime(){
        return this.processingTime;
    }

    synchronized public void addClient(Client c){
        try {
            clients.put(c);
            this.processingTime += c.getProcessingTime();
            logger.append("Client " + c.getId() + " has entered the queue: " + this.getqId() + " at the time " + time + "\n");
            updateQueueStatus();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    synchronized public Client removeClient(){
        Client result = null;
        try {
            result =  clients.take();
        }catch(InterruptedException e){
            e.printStackTrace();
        }
        return result;
    }

    public void updateQueueStatus(){
        String temp = "";
        for(int i=0;i<clients.size();i++){
            temp+="$";
        }
        this.queueState.setText(temp);
    }

    public void stop(){
        exit = true;
    }

    public int getSize(){
        return clients.size();
    }

    public int getqId() {
        return qId;
    }
    public void setqId(int qId) {
        this.qId = qId;
    }

    public double getAverageWaitingTime(){
        int result = 0;
        for (Integer i : waitingTime) {
            result += i;
        }
        return 1.0 * result/waitingTime.size();
    }

    public int getEmtpyQueueTime(){
        int result = 0;
        for(Boolean b : emptyQueue){
            if(b == false)
                result++;
        }
        return result;
    }
}
