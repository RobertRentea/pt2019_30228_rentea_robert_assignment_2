package Model;

import View.InputBox;
import View.SimulatorFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class SimulationManager implements Runnable {

    public int timeLimit;
    public int maxProcessingTime;
    public int minProcessingTime;
    public int maxArrivalTime;
    public int minArrivalTime;
    public int numberOfQueues;
    public int numberOfClients;
    public SelectionPolicy selectionPolicy;
    public int currentTime;

    private SimulatorFrame view;
    private InputBox input;

    private Scheduler scheduler;
    private Queue<Client> generatedClients;

    Thread t;

    public SimulationManager(SimulatorFrame view, InputBox input){
        this.view = view;
        this.input = input;
        this.selectionPolicy = SelectionPolicy.SHORTEST_TIME;
        t = new Thread(this);

    }

    public void setSimulationParameters(int timeLimit, int maxProcessingTime, int minProcessingTime, int maxArrivalTime, int minArrivalTime, int numberOfQueues, int numberOfClients){

        this.timeLimit = timeLimit;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minArrivalTime = minArrivalTime;
        this.numberOfQueues = numberOfQueues;
        this.numberOfClients = numberOfClients;

        view.queuePanel.setLayout(new GridLayout(numberOfQueues, 1));

        for(int i=0;i<numberOfQueues;++i){
            JTextField tf = new JTextField();
            tf.setPreferredSize(new Dimension(200, 24));
            view.queues.add(tf);
        }
        for (JTextField tf :  view.queues) {
            view.queuePanel.add(tf);
        }

        scheduler = new Scheduler(numberOfQueues, 10, view.logger, view.queues);
        scheduler.changeStrategy(selectionPolicy);
        this.generateNRandomClients(minArrivalTime, maxArrivalTime);
    }

    private void generateNRandomClients(int minArrivalTimeBetweenClients, int maxArrivalTimeBetweenClients0){
        generatedClients = new LinkedList<>();
        int arrivalTime = 0;
        for (int i = 0;i<numberOfClients;i++) {
            arrivalTime += (int)(Math.random()*(maxArrivalTimeBetweenClients0 - minArrivalTimeBetweenClients)) + minArrivalTimeBetweenClients;
            int processingTime = (int)(Math.random()*(maxProcessingTime - minProcessingTime)) + minProcessingTime;
            Client c = new Client(arrivalTime, processingTime);
            c.setId(i);
            generatedClients.add(c);
        }

        for (Client c : generatedClients) {
            System.out.println(c);
        }

    }

    @Override
    public void run() {
        currentTime = 0;
        while(currentTime < timeLimit){
            if(generatedClients.size()!=0) {
                if (currentTime == generatedClients.peek().getArrivalTime())
                    scheduler.dispatchClient(generatedClients.remove());
            }
            currentTime++;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
            }

        }
        scheduler.terminateThreads();
        scheduler.printQueueStatistics(timeLimit);
    }




}
