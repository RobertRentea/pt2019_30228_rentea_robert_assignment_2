package Model;

import javax.swing.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Coada> queues;
    private List<Thread> threads;
    private int maxNoQueues;
    private int maxClientsPerQueue;
    private Strategy strategy;
    private JTextArea logger;

    private static DecimalFormat df2 = new DecimalFormat("#.##");

    public Scheduler(int noOfQueues, int maxClientsPerQueue, JTextArea logger, List<JTextField> queueStates){
        this.logger = logger;
        this.queues = new ArrayList<>(noOfQueues);
        this.threads = new ArrayList<>();
        this.maxClientsPerQueue = maxClientsPerQueue;

        for(int i=0;i<noOfQueues;i++){
            Coada q = new Coada(maxClientsPerQueue, logger, queueStates.get(i));
            q.setqId(i);
            this.queues.add(q);
        }

        for (Coada q : queues) {
            threads.add(new Thread(q));
        }
        for (Thread t : threads) {
            t.start();
        }

    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ConcreteStrategyQueue();
        }else if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchClient(Client c){
        strategy.addClient(queues, c);
    }

    public void terminateThreads(){
        for (Coada q:  queues) {
            q.stop();
        }
    }

    public void printQueueStatistics(int maxTime){
        logger.append("\nSimulation statistics: \n");
        for(Coada q: queues){
            logger.append("Q"+q.getqId()+": waiting time = "+df2.format(q.getAverageWaitingTime()) + " empty time: " + q.getEmtpyQueueTime()+"\n");
        }
        logger.append(getPeekHour(maxTime));
    }

    public String getPeekHour(int maxTime){
        int max = Integer.MIN_VALUE;
        int time = 0;
        for(int i = 0;i < queues.get(0).numberOfClientsInQueue.size();++i){
            int x;
            int n = 0;
            for(Coada q : queues){
                n += q.numberOfClientsInQueue.get(i);
            }
            if(n > max){
                max = n;
                time = i;
            }
        }
        return "Peek hour was: " + time + " with " +max+ " clients in queues\n";
    }

    public List<Coada> getQueues() {
        return queues;
    }

    public void setQueues(List<Coada> queues) {
        this.queues = queues;
    }
}
