package Model;

public class Client implements Comparable{
    private int arrivalTime;
    private int processingTime;
    private int startingTime;
    private int finishingTime;
    private int waitingTime;

    private int id;

    public Client(int arrivalTime, int processingTime) {
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    @Override
    public int compareTo(Object o) {
        Client c = (Client) o;
        return c.arrivalTime - this.arrivalTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStartingTime() {
        return startingTime;
    }

    public void setStartingTime(int startingTime) {
        this.startingTime = startingTime;
    }

    public int getFinishingTime() {
        return finishingTime;
    }

    public void setFinishingTime(int finishingTime) {
        this.finishingTime = finishingTime;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime() {
        this.waitingTime = finishingTime - startingTime;
    }

    public String toString(){
        return "C"+this.getId()+": arrivalTime = " + this.getArrivalTime() + " processingTime= " + this.getProcessingTime();
    }
}
