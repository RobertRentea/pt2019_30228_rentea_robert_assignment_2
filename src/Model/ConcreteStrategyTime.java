package Model;

import java.util.List;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addClient(List<Coada> queues, Client c) {
        int min = Integer.MAX_VALUE;
        Coada minQueue = null;
        for (Coada q : queues) {
            if (q.getProcessingTime() < min) {
                min = q.getProcessingTime();
                minQueue = q;
            }
        }
        minQueue.addClient(c);
    }
}
