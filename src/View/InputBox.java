package View;

import Model.SimulationManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InputBox extends JFrame {
    private SimulationManager simManager;

    private JPanel timeLimitPanel;
    private JPanel processingTimePanel;
    private JPanel arrivalTimeBetweenClientsPanel;
    private JPanel numberOfQueuePanel;
    private JPanel numberOfClientsPanel;

    public JTextField timeLimitTF;
    public JTextField minProcessingTimeTF;
    public JTextField maxProcessingTimeTF;
    public JTextField minArrivalTimeTF;
    public JTextField maxArrivalTimeTF;
    public JTextField numberOfQueuesTF;
    public JTextField numberOfClientsTF;

    private JButton Submit;

    public InputBox(){
        this.setSize(400,250);
        this.setLayout(new GridLayout(5,1));
        timeLimitPanel = new JPanel();
        timeLimitPanel.setAlignmentX(LEFT_ALIGNMENT);
        timeLimitPanel.setLayout(new FlowLayout(10, 10, 5));
        JLabel timeLimitLabel = new JLabel("time limit:");
        timeLimitTF = new JTextField();
        timeLimitTF.setPreferredSize(new Dimension(40, 20));
        timeLimitPanel.add(timeLimitLabel);
        timeLimitPanel.add(timeLimitTF);

        processingTimePanel = new JPanel();
        processingTimePanel.setLayout(new FlowLayout(10, 10, 5));
        JLabel minProcessingTimeLabel = new JLabel("min processing time:");
        minProcessingTimeTF = new JTextField();
        minProcessingTimeTF.setPreferredSize(new Dimension(40, 20));
        processingTimePanel.add(minProcessingTimeLabel);
        processingTimePanel.add(minProcessingTimeTF);

        JLabel maxProcessingTimeLabel = new JLabel("max processing time:");
        maxProcessingTimeTF = new JTextField();
        maxProcessingTimeTF.setPreferredSize(new Dimension(40, 20));
        processingTimePanel.add(maxProcessingTimeLabel);
        processingTimePanel.add(maxProcessingTimeTF);

        arrivalTimeBetweenClientsPanel = new JPanel();
        arrivalTimeBetweenClientsPanel.setLayout(new FlowLayout(10, 10, 5));
        JLabel minArrivalTimeLabel = new JLabel("min arrival time:         ");
        minArrivalTimeTF = new JTextField();
        minArrivalTimeTF.setPreferredSize(new Dimension(40, 20));
        arrivalTimeBetweenClientsPanel.add(minArrivalTimeLabel);
        arrivalTimeBetweenClientsPanel.add(minArrivalTimeTF);

        JLabel maxArrivalTimeLabel = new JLabel("max arrival time:         ");
        maxArrivalTimeTF = new JTextField();
        maxArrivalTimeTF.setPreferredSize(new Dimension(40, 20));
        arrivalTimeBetweenClientsPanel.add(maxArrivalTimeLabel);
        arrivalTimeBetweenClientsPanel.add(maxArrivalTimeTF);

        numberOfQueuePanel = new JPanel();
        numberOfQueuePanel.setLayout(new FlowLayout(10, 10, 5));
        JLabel numberOfQueueLabel = new JLabel("number of queues:    ");
        numberOfQueuesTF = new JTextField();
        numberOfQueuesTF.setPreferredSize(new Dimension(40, 20));
        numberOfQueuePanel.add(numberOfQueueLabel);
        numberOfQueuePanel.add(numberOfQueuesTF);

        JLabel numberOfClientsLabel = new JLabel("number of clients:       ");
        numberOfClientsTF = new JTextField();
        numberOfClientsTF.setPreferredSize(new Dimension(40, 20));
        numberOfQueuePanel.add(numberOfClientsLabel);
        numberOfQueuePanel.add(numberOfClientsTF);

        Submit = new JButton("Submit");
        Submit.setBackground(new Color(30, 156, 188));

        this.add(timeLimitPanel);
        this.add(processingTimePanel);
        this.add(arrivalTimeBetweenClientsPanel);
        this.add(numberOfQueuePanel);
        this.add(Submit);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addButtonListener(ActionListener a){
        Submit.addActionListener(a);
    }

}
