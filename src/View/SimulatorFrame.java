package View;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class SimulatorFrame extends JFrame {

    public static final long serialVersionUID = 1L;
    public JPanel panel;
    public int width = 900, height = 600;
    public List<JTextField> queues;
    public JPanel queuePanel;
    public JScrollPane scrollPane;
    public JTextArea logger;

    public SimulatorFrame(int numberOfQueues){
        panel = new JPanel();

        this.queues = new ArrayList<>();
        queuePanel = new JPanel();
        queuePanel.setSize(600, 600);

        scrollPane = new JScrollPane(queuePanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        panel.add(scrollPane);

        JPanel Logger = new JPanel();
        Logger.setLayout(new BorderLayout());
        JLabel label = new JLabel("Logger");
        logger = new JTextArea();
        JScrollPane pane = new JScrollPane(Logger);
        Logger.add(label, BorderLayout.NORTH);
        Logger.add(logger, BorderLayout.CENTER);

        this.setLayout(new GridLayout(1,2));
        this.add(panel);
        this.add(pane);
        this.setSize(width, height);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
    }


}
