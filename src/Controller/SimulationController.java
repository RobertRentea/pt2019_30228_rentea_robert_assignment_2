package Controller;

import Model.SimulationManager;
import View.InputBox;
import View.SimulatorFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimulationController {
    SimulationManager simManager;
    InputBox input;
    SimulatorFrame frame;

    Thread t;

    public SimulationController(SimulationManager simManager, InputBox inputBox, SimulatorFrame frame){
        this.simManager = simManager;
        this.input = inputBox;
        this.frame = frame;
        t = new Thread(simManager);
        input.addButtonListener(new ButtonListener());
    }

    public class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String tmp = input.timeLimitTF.getText();
            int timeLimit = Integer.parseInt(tmp);
            tmp = input.minProcessingTimeTF.getText();
            int minPorcessingTime = Integer.parseInt(tmp);
            tmp = input.maxProcessingTimeTF.getText();
            int maxPorcessingTime = Integer.parseInt(tmp);

            tmp = input.minArrivalTimeTF.getText();
            int minArrivalTime = Integer.parseInt(tmp);
            tmp = input.maxArrivalTimeTF.getText();
            int maxArrivalTime = Integer.parseInt(tmp);

            tmp = input.numberOfClientsTF.getText();
            int numberOfClients = Integer.parseInt(tmp);
            tmp = input.numberOfQueuesTF.getText();
            int numberOfQueues = Integer.parseInt(tmp);

            simManager.setSimulationParameters(timeLimit, maxPorcessingTime, minPorcessingTime, maxArrivalTime, minArrivalTime, numberOfQueues, numberOfClients);
            frame.setVisible(true);
            input.setVisible(false);
            t.start();
        }
    }

    public static void main(String[] args){
        SimulatorFrame view = new SimulatorFrame(1);
        InputBox inputBox = new InputBox();
        SimulationManager simManager = new SimulationManager(view, inputBox);
        SimulationController controller = new SimulationController(simManager, inputBox, view);
    }
}
